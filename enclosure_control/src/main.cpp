/******************************** IMPORTANT **********************************
 * to upload html + images files, place them in data dir beside src (NOT inside)
 * In Platformio use in left vertical menu:
 *    Platformio -> Platform -> Upload Filesystem Image
 *****************************************************************************/

#include <main.h>

/***************************** buttons definition *****************************/

Button buttons[] = {
	(Button){"&PW:", BUTTON1_PIN, 0, 0, "Power"},
	(Button){"&LI:", BUTTON2_PIN, 0, 0, "Light"},
	(Button){"&EA:", BUTTON3_PIN, 0, 0, "Exhaust Air"},
	(Button){"&HE:", BUTTON4_PIN, 0, 0, "Heater"},
	(Button){"&PI:", BUTTON5_PIN, 0, 0, "PI"},
	(Button){"&PR:", BUTTON6_PIN, 0, 0, "Printer"}};

// int threshold = 40;

/***************************** QR code definition *****************************/

// QRCode qrcode;

/***************************** screen definition ******************************/

TFT_eSPI tft = TFT_eSPI(135, 240); // Invoke custom library

/*********************** effectors tracking definition ************************/

Effector effectors[] = {
	(Effector){"B", 0, 0},	// Buzzer
	(Effector){"F", 0, 0},	// cooling Fans PWM
	(Effector){"EV", 0, 0}, // Relay Exhaust Fumes Venting On/Off
	(Effector){"H", 0, 0},	// Relay Enclosure Heater On/Off
	(Effector){"LI", 0, 0}, // Relay Lights On/Off
	(Effector){"PI", 0, 0}, // Relay Raspberry PI Power On/Off
	(Effector){"PT", 0, 0}, // Relay Printer Power On/Off
	(Effector){"PW", 0, 0}	// Relay Power On/Off
};

/************************ sensors tracking definition *************************/



Sensor sensors[] = {
	(Sensor){"G", 0, 0, 0},	  // Gas Printer Box
	(Sensor){"G", 0, 0, 0},	  // Gas Printer Box
	(Sensor){"FP", 0, 0, 0},  // Fire Printer box
	(Sensor){"FE", 0, 0, 0},  // Fire Electronics box
	(Sensor){"HP", 0, 0, 0},  // Humidity Printer box
	(Sensor){"HF", 0, 0, 0},  // Humidity Filament box
	(Sensor){"TEL", 0, 0, 0}, // Temperature Electronics cards
	(Sensor){"TPS", 0, 0, 0}, // Temperature Power Supply
	(Sensor){"TEX", 0, 0, 0}, // Temperature Extrudeur
	(Sensor){"TMA", 0, 0, 0}, // Temperature motor A
	(Sensor){"TMB", 0, 0, 0}, // Temperature motor B
	(Sensor){"T6", 0, 0, 0}	  // Temperature spare
};

/******************************* UART definition*******************************/

HardwareSerial Nano1(1);
HardwareSerial Nano2(2);

/*********************************** Let's go! ********************************/

void setup()
{
	displaySetup();

	uartSetup();

	localFileSystemSetup();

	buttonsSetup();

	serverSetup();

	sensorsSetup();

	// delay(DELAY_MS);

	xTaskCreatePinnedToCore(
		nano1Task,	  /* Function to implement the task */
		"nano1Task ", /* Name of the task */
		4000,		  /* Stack size in words */
		NULL,		  /* Task input parameter */
		5,			  /* Priority of the task */
		NULL,		  /* Task handle. */
		1);			  /* Core where the task should run */

	xTaskCreatePinnedToCore(
		nano2Task,	  /* Function to implement the task */
		"nano2Task ", /* Name of the task */
		4000,		  /* Stack size in words */
		NULL,		  /* Task input parameter */
		5,			  /* Priority of the task */
		NULL,		  /* Task handle. */
		1);			  /* Core where the task should run */
}

void loop()
{
	readButtons();
	// colour = random(0xFFFF);
	// tft.setTextColor(colour);
	// show("No sensor", 20, 100, 4);
	// Serial.println("No Dallas sensor...");
	vTaskDelay(5000 / portTICK_PERIOD_MS);
}

void buttonsSetup(void)
{
	// initialize the button pin as an input:
	int i = 0;
	while (buttons[i].pin)
	{
		pinMode(buttons[i].pin, INPUT);
		i++;
	}
}

void displaySetup(void)
{
	tft.init();
	tft.setRotation(1);
	tft.fillScreen(TFT_BLACK); // else display previous view (prior to shut down)
	tft.setTextColor(TFT_RED);
	// tft.drawCircle(100, 100, 20, TFT_BLUE);
	// show("toto", 0, 0, 4);
}

/**
 * Return true when a value has changed
 */
void extractValue(Sensor *sensor, String s, bool *hasChanged)
{
	Serial.print("Sensor Name [");
	Serial.print(sensor->sensorName);
	Serial.print("] ");
	// Find where in the string
	int start = s.indexOf(sensor->sensorName);
	start = s.indexOf(':', start) + 1;
	// Extract value as string
	int stop = s.indexOf('&', start);
	String valueAsString = s.substring(start, stop);
	Serial.println(valueAsString);
	sensor->lastValue = sensor->value;
	sensor->value = valueAsString.toFloat();

	events.send(valueAsString.c_str(), sensor->sensorName, millis());

	*hasChanged = hasChanged || (sensor->value != sensor->lastValue);
}

// void IRAM_ATTR isr(void *arg)
// {
//   Button *s = static_cast<Button *>(arg);
//   s->numberKeyPresses += 1;
//   s->state = true;
// }

void localFileSystemSetup(void)
{
	if (!SPIFFS.begin(true))
	{
		Serial.println("An Error has occurred while mounting SPIFFS");
		return;
	}
}

void nano1Task(void *pvParameters)
{
	// numberOfFoundSensors = 0;

	// if (!numberOfFoundSensors)
	// {
	//   vTaskDelete(NULL);
	// }

	/* main temptask loop */
	// Serial.println("Nano 1 loop");
	while (1)
	{
		delay(DELAY_MS);
		// String s = "&G:" + String(random(0, 100)) + "&FP:" + String(random(0, 100)) + "&FE:" + String(random(0, 100)) + "&HP:" + String(random(0, 100)) + "&HF:" + String(random(0, 100)) + "&TEL:" + String(random(0, 100)) + "&TPS:" + String(random(0, 100)) + "&TEX:" + String(random(0, 100)) + "&TMA:" + String(random(0, 100)) + "&TMB:" + String(random(0, 100)) + "&T6:" + String(random(0, 100));
		// Nano1.println(s);

		while (Nano1.available() > 0)
		{
			String s = Nano1.readStringUntil('\n');
			Serial.println(s);
			colour = random(0xFFFF);
			tft.setTextColor(colour);
			show(s, 40, 40, 2);
			bool hasChanged = false;
			int i = 0;
			while (sensors[i].sensorName)
			{
				extractValue(&sensors[i], s, &hasChanged);
				i++;
			}
			if (hasChanged)
			{
				// TODO
			}
			break;
		}
	}
}

void nano2Task(void *pvParameters)
{
	// numberOfFoundSensors = 0;

	// if (!numberOfFoundSensors)
	// {
	//   vTaskDelete(NULL);
	// }

	/* main temptask loop */

	while (1)
	{
		delay(1.5 * DELAY_MS);
		bool instructNano = false;

		int i = 0;
		while (buttons[i].name)
		{
			// Read button states and compare last from actual to know if action is needed
			if (buttons[i].state != buttons[i].lastState)
			{
				instructNano = true;
				Nano2.print(buttons[i].name);
				Nano2.print(buttons[i].state);
				buttons[i].lastState = buttons[i].state; // to clear pending actions until user act
			}
			i++;
		}

		i = 0;
		while (effectors[i].name)
		{
			if (effectors[i].value != effectors[i].lastValue)
			{
				instructNano = true;
				Nano2.print(effectors[i].name);
				Nano2.print(effectors[i].value);
				effectors[i].lastValue = effectors[i].value;
			}
			i++;
		}

		if (instructNano)
		{
			Nano2.flush();
		}

		// Nano2.println("test vers nano 2 &");
		// while (Nano2.available() > 0)
		// {
		// 	String s = Nano2.readStringUntil('&');
		// 	Serial.println(s);
		// 	colour = random(0xFFFF);
		// 	tft.setTextColor(colour);
		// 	show(s, 70, 70, 2);
		// }
	}
}

void processEffector(String paramName, String paramValue)
{
	// EEPROM.write(address, value);
	// EEPROM.commit();
	// EEPROM.read(address);

	// Keep track of operation
	int i = 0;
	while (effectors[i].name)
	{
		if (effectors[i].name == paramName.c_str())
		{
			effectors[i].lastValue = effectors[i].value;
			effectors[i].value = paramValue.toFloat();
		}
		i++;
	}

	// Send back to user interface
	events.send(paramValue.c_str(), paramName.c_str(), millis());
}

void processSettings(String paramName, String paramValue)
{
	//var settingList = [ 'PI_ON_API', 'PI_OFF_API', 'PR_ON_API', 'PR_OFF_API', 'EX_ON_DURATION' ];

	events.send(paramValue.c_str(), paramName.c_str(), millis());
}

void readButton(Button *b)
{
	int state = digitalRead(b->pin);
	if (state != b->state)
	{
		b->state = state;
		b->lastState = !state; // Which will tell nano2Task to take an action
	}
}

void readButtons(void)
{
	int i = 0;
	while (buttons[i].name)
	{
		readButton(&buttons[i]);
		i++;
	}
}

void sensorsSetup(void)
{
	int i = 0;
	while (sensors[i].sensorName)
	{
		sensors[i].value = -99.9;
		i++;
	}
}

void serverSetup(void)
{
	Serial.printf("Connecting to '%s' with password '%s'\n", ssid, password);
	WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
	WiFi.mode(WIFI_STA);
	WiFi.setHostname(hostname);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED)
	{
		vTaskDelay(250 / portTICK_PERIOD_MS);
		Serial.print(".");
		colour = random(0xFFFF);
		tft.drawCircle(150, 90, 20, colour);
	}
	Serial.println();

	// Handle Web Server
	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
		// request->send_P(200, "text/html", index_html, processor);
		// request->send_P(200, "text/html", index_html);
		request->send(SPIFFS, "/mainPage.html");
	});
	server.on("/settings", HTTP_GET, [](AsyncWebServerRequest *request) {
		// request->send_P(200, "text/html", settings_html);
		request->send(SPIFFS, "/settings.html");
	});
	server.on("/effector", HTTP_GET, [](AsyncWebServerRequest *request) {
		// GET input1 value on <ESP_IP>/setting?value=<inputMessage>
		if (request->hasParam(PARAM_INPUT) && request->hasParam(PARAM_NAME))
		{
			processEffector(request->getParam(PARAM_NAME)->value(), request->getParam(PARAM_INPUT)->value());
			String s = "Received: " + request->getParam(PARAM_NAME)->value() + " -> " + request->getParam(PARAM_INPUT)->value();
			Serial.println(s);
			show(s, 40, 40, 2, true);
		}
		else
		{
			Serial.println("No message received");
		}
		request->send(200, "text/plain", "OK");
	});
	server.on("/setting", HTTP_GET, [](AsyncWebServerRequest *request) {
		// GET input1 value on <ESP_IP>/setting?value=<inputMessage>
		if (request->hasParam(PARAM_INPUT) && request->hasParam(PARAM_NAME))
		{
			processSettings(request->getParam(PARAM_NAME)->value(), request->getParam(PARAM_INPUT)->value());
			Serial.println("Received: " + request->getParam(PARAM_NAME)->value() + " -> " + request->getParam(PARAM_INPUT)->value());
		}
		else
		{
			Serial.println("No message received");
		}
		request->send(200, "text/plain", "OK");
	});

	server.onNotFound([](AsyncWebServerRequest *request) {
		request->send(404, "text/plain", "Not found");
	});

	// Handle Web Server Events
	events.onConnect([](AsyncEventSourceClient *client) {
		if (client->lastId())
		{
			Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
		}
		// send event with message "hello!", id current millis
		// and set reconnect delay to 1 second
		client->send("hello!", NULL, millis(), 10000);
	});

	server.addHandler(&events);
	server.begin();

	ip.concat(WiFi.localIP().toString());
	ip.concat("/");

	show(ip, 10, 10, 4, true);
	// display_QRcode(5, 5, 6, 1, 0, ip.c_str());
	// qrcode.create(ip.c_str());
	Serial.print("\n\nBrowse to: ");
	Serial.print(ip);
	Serial.println(" to get temperature readings.\n");
}

void show(const String &string, int32_t poX, int32_t poY, uint8_t font, bool newScreen)
{
	if (newScreen)
	{
		tft.fillScreen(TFT_BLACK); // else display previous view
	}
	tft.drawString(string, poX, poY, font);
}

void uartSetup(void)
{
	Serial.begin(9600);
	Serial.println("\n\nSimple DS18B20 Dallas sensor webserver example.\n");
	Nano1.begin(9600, SERIAL_8N1, RX1_PIN, TX1_PIN);
	Nano2.begin(9600, SERIAL_8N1, RX2_PIN, TX2_PIN);
}
