/******************************** IMPORTANT **********************************
 * to upload html + images files, place them in data dir beside src (NOT inside)
 * In Platformio use in left vertical menu:
 *    Platformio -> Platform -> Upload Filesystem Image
 * https://github.com/Xinyuan-LilyGO/TTGO-T-Display
 *****************************************************************************/

#include <Arduino.h>
#include <HardwareSerial.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "SPIFFS.h"
//#include <EEPROM.h>
#include <SPI.h>
#include <TFT_eSPI.h> // Hardware-specific library
// #include <qrcode.h>

#define DELAY_MS 2000
#define MAX_NUMBER_OF_SENSORS 7 // maximum number of Dallas sensors
// #define TEMPLATE_PLACEHOLDER '$'
#define TFT_GREY 0x5AEB

/****************************** EEPROM definition *****************************/
// define the number of bytes you want to access
//#define EEPROM_SIZE 1

/******************************* Pins definition ******************************/
// the pins 4, 5, 16, 18, 19 and 23 are connected to the built-in tft screen (SPI bus)
#define BUTTON1_PIN T2 // GPIO2, Power
#define BUTTON2_PIN T3 // GPIO15, Light
#define BUTTON3_PIN T4 // GPIO13, Exhaust fan
#define BUTTON4_PIN T5 // GPIO12, Heater
#define BUTTON5_PIN T8 // GPIO33, PI
#define BUTTON6_PIN T9 // GPIO32, Printer
#define TX1_PIN 25     // GPIO25
#define RX1_PIN 26     // GPIO26
#define TX2_PIN 17     // GPIO17
#define RX2_PIN 27     // GPIO27

/***************************** buttons definition *****************************/

typedef struct
{
    const char *name;
    const uint8_t pin;
    // uint32_t numberKeyPresses;
    int lastState;
    int state;
    const char *label;
} Button;

/***************************** screen definition ******************************/

unsigned int colour = 0;

/*********************** effectors tracking definition ************************/

typedef struct
{
    const char *name;
    float value;
    float lastValue;
} Effector;

/************************ sensors tracking definition *************************/

typedef struct
{
    const char *sensorName;
    byte addr[8];
    float value;
    float lastValue;
} Sensor;

/******************************* web definition *******************************/

const char *hostname = "enclosure.local";
const char *ssid = "Livebox-5279";
const char *password = "2437EE662F9F3889347F2AA777";
AsyncWebServer server(80);
AsyncEventSource events("/events");

String ip = "http://";
String settings = "";

// String processor(const String &var)
// {
//   //Serial.println(var);
//   if (var == "TEL")
//   {
//     return String(sensor6.value); // Temperature Electronics
//   }
//   else if (var == "TPS")
//   {
//     return String(sensor7.value); // Temperature Power Supply
//   }
//   else if (var == "TEX")
//   {
//     return String(sensor8.value); // Temperature Extrudeur
//   }
//   else if (var == "TMA")
//   {
//     return String(sensor9.value); // Temperature motor A
//   }
//   else if (var == "TMB")
//   {
//     return String(sensor10.value); // Temperature motor B
//   }
//   else if (var == "T6")
//   {
//     return String(sensor11.value); // Temperature spare
//   }
//   else if (var == "HP")
//   {
//     return String(sensor4.value); // Humidity Printer box
//   }
//   else if (var == "HF")
//   {
//     return String(sensor5.value); // Humidity Spools box
//   }
//   else if (var == "FP")
//   {
//     return String(sensor2.value); // Fire Printer box
//   }
//   else if (var == "FE")
//   {
//     return String(sensor3.value); // Fire Electronics box
//   }
//   else if (var == "G")
//   {
//     return String(sensor1.value); // Gas Printer Box
//   }
//   else if (var == "FANS_SPEED")
//   {
//     return String(effector2.value);
//   }
//   return "";
// }

const char *PARAM_INPUT = "value";
const char *PARAM_NAME = "name";

/************************ invocations Not to use .ino file ********************/

void buttonsSetup(void);
void displaySetup(void);
// void display_QRcode(int offset_x, int offset_y, int element_size, int QRsize, int ECC_Mode, const char *Message);
void extractValue(Sensor sensor, String s, bool &hasChanged);
// void IRAM_ATTR isr(void *arg);
void localFileSystemSetup(void);
void nano1Task(void *pvParameters);
void nano2Task(void *pvParameters);
void processEffector(String paramName, String paramValue);
void processSettings(String paramName, String paramValue);
void readButton(void *arg);
void readButtons(void);
void sensorsSetup(void);
void serverSetup(void);
void show(const String &string, int32_t poX, int32_t poY, uint8_t font, bool newScreen = false);
void uartSetup(void);
