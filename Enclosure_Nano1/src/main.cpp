/******************************************************************************
 *
 * Output string on UART is as such:
 *  &G:xx.x&F1:xx.x&F2:xx.x&H1:xx.x&H2:xx.x&T1:xx.x&T2:xx.x&....
 * where xx.x is a float written as string from 300.0 ppm to 10000.0 ppm
 * Pour réaliser la calibration, inutile de réinventer la roue, nous allons utiliser le code mis à disposition par Sandbox Electronics qui permet de calibrer le capteur pour détecter la présence de GPL, de CO (Monoxide de carbone) ou de fumées dans l’atmosphère environnante. Créez un projet Arduino et collez le code ci-dessous. Ce programme réalise la calibration de la résistance R0 seul. Pour cela, assurez vous que le capteur soit placé dans un air propre lorsque vous démarrez le programme.
 *
******************************************************************************/

#include <Arduino.h>
#include <Wire.h>
// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <MQUnifiedsensor.h>

/******************************* Pins definition ******************************/

#define DHT1_PIN 7     // Digital pin connected to the DHT sensor (digital), D6
#define DHT2_PIN 8     // Digital pin connected to the DHT sensor (digital), D7
#define ONE_WIRE_BUS 4 // OneWire Dallas sensors bus pin (digital)
#define MQ2_PIN A0     // Gas sensor pin (analog) physical pin 19
#define FIRE1_PIN A2   // Gas sensor pin (analog) physical pin 21
#define FIRE2_PIN A1   // Gas sensor pin (analog) physical pin 20

/******************************* DHT definition *******************************/

// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHT_PIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT_Unified dht1(DHT1_PIN, DHT11);
DHT_Unified dht2(DHT2_PIN, DHT11);
typedef struct
{
  const char *name;
  DHT_Unified *device;
  const char *label;
  const uint8_t pin;
} Humidity;

Humidity humiditySensors[] = {
    {"F", &dht1, "Filament", DHT1_PIN},
    {"P", &dht2, "Printer", DHT2_PIN}};

byte numberOfSensorsHumidity = sizeof(humiditySensors) / sizeof(Humidity);
uint32_t humidityDelayMS = 0;

/******************************* DS18B20 definition ***************************/

#define MAX_NUMBER_OF_SENSORS 7 // maximum number of Dallas sensors
#define INPUT_SIZE 30           // Calculate based on max input size expected for one command

/**************************** Fire sensor definition **************************/

typedef struct
{
  const char *name;
  const char *label;
  const uint8_t pin;
  int lastState;
  int state;
  bool invert;
} Fire;

Fire fireSensors[] = {
    {"FP", "fire printer", FIRE1_PIN, 0, 0, false},
    {"FE", "fire electronics", FIRE2_PIN, 0, 0, true}};

byte numberOfSensorsFire = sizeof(fireSensors) / sizeof(Fire);

/***************************** Gaz sensor definition **************************/

/**
  * RS / R0 = 9.83 ppm
  * Board                   ("Arduino UNO")
  * Voltage_Resolution      (5)
  * ADC_Bit_Resolution      (10)   For arduino UNO/MEGA/NANO
  * Pin                     (A2)   Analog input 19 of nano
  * Type                    ("MQ-2") //MQ2
  */
#define RatioMQ2CleanAir (9.83)
MQUnifiedsensor MQ2("Arduino NANO", 5, 10, MQ2_PIN, "MQ-2");

/**************************** Temperature definition **************************/

char *tempSensorLabels[] = {(char *)"TEL", (char *)"TPS", (char *)"TEX", (char *)"TMA", (char *)"TMB", (char *)"T6"};
// char *tempSensors[10];
// tempSensors[0] = (char *)"TEX";
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature tempSensors(&oneWire);
DeviceAddress deviceAddress; // One Wire sensor address
uint8_t numberOfFoundTempSensors;

/************************ invocations Not to use .ino file ********************/

void establishContact(void);
void fireSetup(void);
void gasSetup(void);
void humiditySetup(void);
void printAddress(DeviceAddress deviceAddress);
void readFireDetection(Fire *fireSensor);
void readFires(void);
void readGas(void);
void readHumidities(void);
void readHumidity(Humidity *hum);
void readSensors(void);
void readTemp(uint8_t index);
void readTemperatures(void);
void setSensorsConfigFromUser(void);
void tempSensorsSetup(void);

/*********************************** Let's go! ********************************/

void setup()
{
  // Send a byte to establish contact until receiver responds
  establishContact();

  // Get initial user settings on sensors labels assignation
  setSensorsConfigFromUser();

  // Gas setup
  gasSetup();

  // Fire sensors setup
  fireSetup();

  // Humidity setUp
  humiditySetup();

  // Temperature setup
  tempSensorsSetup();
}

void loop()
{
  // Wait a few seconds between measurements (before any command, to be applied also after setup)
  delay(humidityDelayMS * 2);

  readSensors();
}

void establishContact(void)
{
  Serial.begin(9600);

  while (Serial.available() <= 0)
  {
    Serial.println(F("Nano 1 ready...")); // send an initial string
    delay(300);
  }
}

void fireSetup(void)
{
  pinMode(FIRE1_PIN, INPUT);
  pinMode(FIRE2_PIN, INPUT);
}

void gasSetup(void)
{
  // Serial.print("MQ2: ");
  // Serial.println(analogRead(MQ2_PIN));
  //Set math model to calculate the PPM concentration and the value of constants
  MQ2.setRegressionMethod(1); //_PPM =  a*ratio^b
  MQ2.setA(36974);
  MQ2.setB(-3.109); // Configurate the equation values to read LPG concentration
  /*
    Exponential regression:
    Gas    | a      | b
    H2     | 987.99 | -2.162
    LPG    | 574.25 | -2.222
    CO     | 36974  | -3.109
    Alcohol| 3616.1 | -2.675
    Propane| 658.71 | -2.168
  */

  /*****************************  MQ Init ********************************************/
  //Remarks: Configure the pin of arduino as input.
  /************************************************************************************/
  MQ2.init();
  /*
    //If the RL value is different from 10K please assign your RL value with the following method:
    MQ2.setRL(10);
  */
  /*****************************  MQ CAlibration ********************************************/
  // Explanation:
  // In this routine the sensor will measure the resistance of the sensor supposing before was pre-heated
  // and now is on clean air (Calibration conditions), and it will setup R0 value.
  // We recomend execute this routine only on setup or on the laboratory and save on the eeprom of your arduino
  // This routine not need to execute to every restart, you can load your R0 if you know the value
  // Acknowledgements: https://jayconsystems.com/blog/understanding-a-gas-sensor
  // Serial.print("Calibrating please wait.");
  float calcR0 = 0;
  for (int i = 1; i <= 10; i++)
  {
    MQ2.update(); // Update data, the arduino will be read the voltage on the analog pin
    calcR0 += MQ2.calibrate(RatioMQ2CleanAir);
    // Serial.print(".");
  }
  MQ2.setR0(calcR0 / 10);
  // Serial.println("  done!.");

  if (isinf(calcR0))
  {
    Serial.println("Warning: Connection issue founded, R0 is infite (Open circuit detected) please check your wiring and supply");
    while (1)
      ;
  }
  if (calcR0 == 0)
  {
    Serial.println("Warning: Connection issue founded, R0 is zero (Analog pin with short circuit to ground) please check your wiring and supply");
    while (1)
      ;
  }

  // MQ2.serialDebug(true);
}

void humiditySetup(void)
{
  for (byte i = 0; i < numberOfSensorsHumidity; i++)
  {
    DHT_Unified *dht = humiditySensors[i].device;
    sensor_t sensor;

    dht->begin();

    // Print temperature sensor details.
    // (humiditySensors[i].device)->temperature().getSensor(&sensor);
    // Serial.println(F("------------------------------------"));
    // Serial.println(F("Temperature Sensor"));
    // Serial.print(F("Sensor Type: "));
    // Serial.println(sensor.name);
    // Serial.print(F("Driver Ver:  "));
    // Serial.println(sensor.version);
    // Serial.print(F("Unique ID:   "));
    // Serial.println(sensor.sensor_id);
    // Serial.print(F("Max Value:   "));
    // Serial.print(sensor.max_value);
    // Serial.println(F("°C"));
    // Serial.print(F("Min Value:   "));
    // Serial.print(sensor.min_value);
    // Serial.println(F("°C"));
    // Serial.print(F("Resolution:  "));
    // Serial.print(sensor.resolution);
    // Serial.println(F("°C"));
    // Serial.println(F("------------------------------------"));
    // Print humidity sensor details.
    dht->humidity().getSensor(&sensor);
    humidityDelayMS = sensor.min_delay / 1000;
    // Serial.print(F("Humidity Sensor for: "));
    // Serial.print(humiditySensors[i].label);
    // Serial.print(F(" on pin: "));
    // Serial.println(humiditySensors[i].pin);
    // Serial.print(F("Sensor Type: "));
    // Serial.println(sensor.name);
    // Serial.print(F("Driver Ver:  "));
    // Serial.println(sensor.version);
    // Serial.print(F("Unique ID:   "));
    // Serial.println(sensor.sensor_id);
    // Serial.print(F("Max Value:   "));
    // Serial.print(sensor.max_value);
    // Serial.println(F("%"));
    // Serial.print(F("Min Value:   "));
    // Serial.print(sensor.min_value);
    // Serial.println(F("%"));
    // Serial.print(F("Resolution:  "));
    // Serial.print(sensor.resolution);
    // Serial.println(F("%"));
    // Serial.println(F("------------------------------------"));
    // Set delay between sensor readings based on last sensor details.
  }
}

void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16)
      Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

void readFireDetection(Fire *fireSensor)
{
  // read the sensor on analog A0:
  bool sensorReading = digitalRead(fireSensor->pin) ^ fireSensor->invert;
  // Serial.print("Fire : ");
  // Serial.println(!sensorReading);
  // Serial.print('\n');
  Serial.print(F("&")); // send to ESP32
  Serial.print(fireSensor->name);
  Serial.print(F(":"));
  Serial.print(sensorReading); // SensorReading is reversed
  // Serial.println('\n');
}

void readFires(void)
{
  for (byte i = 0; i < numberOfSensorsFire; i++)
  {
    readFireDetection(&fireSensors[i]);
  }
}

void readGas(void)
{
  MQ2.update();               // Update data, the arduino will be read the voltage on the analog pin
  float s = MQ2.readSensor(); // Sensor will read PPM concentration using the model and a and b values setted before or in the setup
  if (isnan(s))
  {
    s = -99;
  }
  else
  {
    s = 100 * s / 1023; // Make sure it return a percent.
  }
  // Serial.println(s);
  Serial.print(F("&G:")); // send to ESP32
  Serial.print(s, 0);
}

void readHumidities(void)
{
  for (byte i = 0; i < numberOfSensorsHumidity; i++)
  {
    readHumidity(&humiditySensors[i]);
  }
}

void readHumidity(Humidity *hum)
{
  DHT_Unified *dht = hum->device;
  sensors_event_t event;

  // // Get temperature event and print its value.
  // dht->temperature().getEvent(&event);
  // Serial.print(F("&T")); // send to ESP32
  // Serial.print(hum->name);
  // Serial.print(F(":"));
  // if (isnan(event.temperature))
  // {
  //   Serial.print("-99");
  // }
  // else
  // {
  //   Serial.print(event.temperature, 1);
  // }

  // Humidity
  dht->humidity().getEvent(&event);
  Serial.print(F("&H")); // send to ESP32
  Serial.print(hum->name);
  Serial.print(F(":"));
  if (isnan(event.relative_humidity))
  {
    Serial.print("-99");
  }
  else
  {
    Serial.print(event.relative_humidity, 1);
  }
}

// sending to ESP32 by printing on Serial port
void readSensors(void)
{
  readHumidities();
  readTemperatures();
  readGas();
  readFires();
  Serial.print('\n'); // Useful for ESP to digest
  Serial.flush();
}

void readTemp(uint8_t index)
{
  // Serial.print(F("Temperature for the device "));
  // Serial.print(index);
  // Serial.print(F(" at address: "));
  // tempSensors.getAddress(deviceAddress, index);
  // printAddress(deviceAddress);
  // Serial.print(F(" is: "));
  float t = tempSensors.getTempCByIndex(index);
  if (isnan(t))
  {
    t = -99;
  }

  Serial.print('&'); // send to ESP32
  Serial.print((char)tempSensors.getUserDataByIndex(index));
  // Serial.print(tempSensors.getUserDataByIndex(index));
  Serial.print(':');
  Serial.print(t, 1);
}

void setSensorsConfigFromUser(void)
{
  // int size;

  // if (sizeof(tempSensors) > 0)
  // {
  //   delete[] tempSensors;
  // }
  // char **tempSensors = new char *[size];
}

void readTemperatures(void)
{
  uint8_t index = 0;
  String value;
  for (byte i = 0; i < numberOfFoundTempSensors; i++)
  {
    readTemp(index);
    index++;
  }
}

void tempSensorsSetup(void)
{
  tempSensors.begin();

  // count devices
  numberOfFoundTempSensors = tempSensors.getDeviceCount();
  // Serial.print("#devices: ");
  // Serial.println(numberOfFoundTempSensors);

  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  // Serial.print("Requesting temperatures...");
  tempSensors.requestTemperatures(); // Send the command to get temperatures
  // Serial.println("DONE");
  String labels = "ABC";
  for (byte index = 0; index < numberOfFoundTempSensors; index++)
  {
    tempSensors.setUserDataByIndex(index, labels[index]); // TODO change for user defined labelling order
    // readTemp(index);
  }
}
